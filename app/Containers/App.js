import React from 'react';
import AppNavigation from './AppNavigation';
import {WebSocketProvider} from './WebSocketContext';

const App = () => {
  return (
    <WebSocketProvider url={'wss://ws-postman.eu-gb.mybluemix.net/ws/'}>
      <AppNavigation />
    </WebSocketProvider>
  );
};

export default App;
