import React from 'react';
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NativeBaseProvider} from 'native-base';
import P1Container from './P1Container';
import P2Container from './P2Container';
import P3Container from './P3Container';
import P4Container from './P4Container';

const NativeStack = createNativeStackNavigator();

const AppNavigation = () => {
  return (
    <NativeBaseProvider>
      <NavigationContainer>
        <NativeStack.Navigator>
          <NativeStack.Group>
            <NativeStack.Screen name="P1" component={P1Container} />
            <NativeStack.Screen name="P2" component={P2Container} />
            <NativeStack.Screen name="P3" component={P3Container} />
            <NativeStack.Screen name="P4" component={P4Container} />
          </NativeStack.Group>
        </NativeStack.Navigator>
      </NavigationContainer>
    </NativeBaseProvider>
  );
};

export default AppNavigation;
