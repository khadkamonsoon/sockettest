import React, {useContext} from 'react';
import {Text, Pressable} from 'native-base';
import {WebSocketContext} from './WebSocketContext';

const P1Container = ({navigation}) => {
  const {webSocket, message} = useContext(WebSocketContext);

  console.log('message 1 : ', message);

  return (
    <Pressable
      onPress={() => {
        navigation.navigate('P2');
      }}
      flex={1}
      bg="amber.100">
      <Text>Hello</Text>
    </Pressable>
  );
};

export default P1Container;
