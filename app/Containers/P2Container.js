import React, {useContext} from 'react';
import {Text, Pressable} from 'native-base';
import {WebSocketContext} from './WebSocketContext';

const P2Container = ({navigation}) => {
  const {webSocket, message} = useContext(WebSocketContext);

  console.log('message 2 : ', message);

  return (
    <Pressable
      onPress={() => {
        navigation.navigate('P3');
      }}
      flex={1}
      bg="red.100">
      <Text>Hello</Text>
    </Pressable>
  );
};

export default P2Container;
