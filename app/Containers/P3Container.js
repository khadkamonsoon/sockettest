import React, {useContext} from 'react';
import {Text, Pressable} from 'native-base';
import {WebSocketContext} from './WebSocketContext';

const P3Container = ({navigation}) => {
  const {webSocket, message} = useContext(WebSocketContext);

  console.log('message 3 : ', message);

  return (
    <Pressable
      onPress={() => {
        navigation.navigate('P4');
      }}
      flex={1}
      bg="yellow.100">
      <Text>Hello</Text>
    </Pressable>
  );
};

export default P3Container;
