import React, {useContext} from 'react';
import {Pressable, Text} from 'native-base';
import {WebSocketContext} from './WebSocketContext';

const P4Container = ({navigation}) => {
  const {webSocket, message} = useContext(WebSocketContext);

  console.log('message 4 : ', message);

  return (
    <Pressable
      onPress={() => {
        navigation.navigate('P1');
      }}
      flex={1}
      bg="pink.100">
      <Text>Hello</Text>
    </Pressable>
  );
};

export default P4Container;
