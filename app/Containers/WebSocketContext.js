import React, {createContext, useState, useEffect} from 'react';
import WebSocket from 'react-native-websocket';

export const WebSocketContext = createContext(null);

export const WebSocketProvider = ({url, children}) => {
  const [webSocket, setWebSocket] = useState(null);
  const [messages, setMessages] = useState([]);

  const onOpen = () => {
    setWebSocket(new WebSocket(url));
  };

  const onMessage = event => {
    setMessages([...messages, event.data]);
  };

  return (
    <WebSocketContext.Provider value={{webSocket, messages}}>
      <WebSocket url={url} onOpen={onOpen} onMessage={onMessage} />
      {children}
    </WebSocketContext.Provider>
  );
};

export const WebSocketConsumer = WebSocketContext.Consumer;
